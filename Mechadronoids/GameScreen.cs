﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Mechadronoids
{
    internal class GameScreen
    {
        Texture2D background;
        Territory selectedTerritory;
        Tooltip tooltip;
        TooltipStateMachine tooltipStateMachine;
        Cosmos cosmos;
        Input input;
        Trigger tooltipTrigger;

        public GameScreen(Texture2D gameBackground, Tooltip gameTooltip, StarAssets starAssets, Command nextTurn, Command exit, Command click)
        {
            background = gameBackground;
            selectedTerritory = null;
            tooltip = gameTooltip;
            tooltipStateMachine = new TooltipStateMachine();
            tooltipTrigger = new Trigger();

            Command command_deselect = new Command(DeselectStar);
            TargetedCommand command_select = new TargetedCommand(SelectStar);

            cosmos = new Cosmos(starAssets, tooltipTrigger);
            input = new Input(command_deselect, nextTurn, exit, command_select, click);
        }

        private void DrawBackground(SpriteBatch spriteBatch)
        {
            Vector2 game_map_pos = new Vector2(0, 0);
            spriteBatch.Draw(background,
                game_map_pos,
                Color.White);
        }

        private void DeselectStar()
        {
            selectedTerritory = null;
        }

        private void SelectStar(Territory star)
        {
            if (selectedTerritory == null)
            {
                if (Faction.player_fac.owned_stars.Contains(star))
                {
                    if (star != Faction.player_fac.Capital)
                    {
                        selectedTerritory = star;
                    }
                }
                else
                {
                    Faction.player_fac.ClaimStar(star);
                }
            }
            else
            {
                if (Faction.player_fac.owned_stars.Contains(selectedTerritory))
                {
                    if (Faction.player_fac.owned_stars.Contains(star))
                    {
                        selectedTerritory.StarIndustry.OutputTarget = star.StarIndustry;
                    }
                    DeselectStar();
                }
            }
        }

        private SpriteBatch DrawFaction(
            SpriteBatch spriteBatch,
            SpriteFont font,
            Vector2 mouse_pos,
            Faction faction)
        {
            string money_string = ((int)faction.Money).ToString();
            string text = "Money: " + money_string;
            Vector2 text_coords = new Vector2(900, 1000);
            faction.Text_obj.Draw(spriteBatch, text_coords, text);

            if (faction.Text_obj.IsHovering(text_coords, mouse_pos, text))
            {
                tooltipStateMachine.tooltipStatus = StarAssets.Tooltip_status.Money;
            }

            return spriteBatch;
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 mouse_vec, SpriteFont smallFont, Matrix res_scale)
        {
            input.HandleInput(res_scale);
            DrawBackground(spriteBatch);

            cosmos.DrawStars(spriteBatch, mouse_vec, selectedTerritory, smallFont);

            DrawFaction(spriteBatch, smallFont, mouse_vec, Faction.player_fac);

            if (selectedTerritory != null)
            {
                tooltipStateMachine.tooltipStatus = StarAssets.Tooltip_status.Selected;
            }

            tooltipStateMachine.UpdateTooltipStatus(spriteBatch, mouse_vec, tooltip, selectedTerritory, tooltipTrigger);

            if (tooltip.active)
            {
                tooltip.Draw();
            }
        }
    }
    
    internal class Input
    {
        Command deselectCommand;
        Command nextTurnCommand;
        Command exitCommand;
        TargetedCommand selectCommand;
        Command playClickCommand;
        bool canClick;
        bool canPressSpace;

        public Input(Command deselect_Command, Command next_turn, Command exit_cmd, TargetedCommand select_cmd, Command play_click_cmd)
        {
            deselectCommand = deselect_Command;
            nextTurnCommand = next_turn;
            exitCommand = exit_cmd;
            selectCommand = select_cmd;
            playClickCommand = play_click_cmd;
            canClick = true;
            canPressSpace = true;
        }

        private void HandleRightClick(Vector2 m_vec)
        {
            bool found_match = false;
            foreach (Territory node in Territory.territoryList)
            {
                double dist = node.System.DistanceFrom(m_vec);
                if (dist < 30)
                {
                    node.Invest(Faction.player_fac);
                    found_match = true;
                    break;
                }
            }

            if (found_match == false)
            {
                deselectCommand.Execute();
            }

            canClick = false;
        }

        private void HandleMouseInput(MouseState m_state, Matrix res_scale)
        {

            Point m_point = m_state.Position;
            Vector2 m_vec_raw = new Vector2(m_point.X, m_point.Y);
            Vector2 m_vec = Vector2.Transform(m_vec_raw, Matrix.Invert(res_scale));
            if ((m_state.LeftButton == ButtonState.Pressed) && canClick)
            {
                canClick = false;

                foreach (Territory node in Territory.territoryList)
                {
                    double dist = node.System.DistanceFrom(m_vec);
                    if (dist < 30)
                    {

                        playClickCommand.Execute();
                        selectCommand.Execute(node);
                        break;
                    }
                }
            }
            else if ((m_state.RightButton == ButtonState.Pressed) && canClick)
            {
                HandleRightClick(m_vec);
            }
            else if (!canClick)
            {
                if ((m_state.LeftButton == ButtonState.Released) && (m_state.RightButton == ButtonState.Released))
                {
                    canClick = true;
                }
            }
        }

        public void HandleInput(Matrix res_scale)
        {

            //Handler keyboard input
            KeyboardState k_state = Keyboard.GetState();

            if (k_state.IsKeyDown(Keys.Escape))
                exitCommand.Execute();
            else if ((k_state.IsKeyDown(Keys.Space)) && canPressSpace)
            {
                nextTurnCommand.Execute();
                canPressSpace = false;
            }
            else if ((k_state.IsKeyUp(Keys.Space)) && !canPressSpace)
            {
                canPressSpace = true;
            }

            //Handle Mouse Input
            MouseState m_state = Mouse.GetState();
            HandleMouseInput(m_state, res_scale);
        }
    }
    
    public class StarAssets
    {
        public static StarAssets starAssets;

        public readonly Texture2D star_texture;
        public readonly Vector2 star_texture_origin;
        public readonly Texture2D capital_star_texture;
        public readonly Texture2D hover_star_texture;
        public readonly Texture2D pixel_texture;
        public readonly Texture2D selected_star_texture;

        public readonly Tooltip tooltip;
        public readonly Text text;

        public enum Tooltip_status
        {
            Inactive,
            Top_number,
            Star,
            Claim,
            Output,
            Selected,
            Money
        }

        public StarAssets(
            Microsoft.Xna.Framework.Content.ContentManager Content,
            GraphicsDevice graphicsDevice,
            Tooltip t_tip,
            Text txt)
        {
            star_texture = Content.Load<Texture2D>("star");
            star_texture_origin = new Vector2(-5, -5);
            capital_star_texture = Content.Load<Texture2D>("capital");
            hover_star_texture = Content.Load<Texture2D>("selection");
            selected_star_texture = Content.Load<Texture2D>("selected");
            Texture2D pixel = new Texture2D(graphicsDevice, 1, 1);
            pixel.SetData(new[] { Color.White });
            pixel_texture = pixel;
            tooltip = t_tip;
            text = txt;
        }
    }
    
    internal class Star
    {
        StarAssets assets;
        Trigger tooltipTrigger;

        public Star(StarAssets starAssets, Trigger tooltipTrig)
        {
            assets = starAssets;
            tooltipTrigger = tooltipTrig;
        }

        private void DrawOutput(SpriteBatch spriteBatch, SpriteFont font, Vector2 mouse_vec, Vector2 text_coords, Territory territory)
        {
            string node_output = ((int)territory.StarIndustry.OutboundPower).ToString();
            Vector2 output_str_length = font.MeasureString(node_output);
            text_coords.Y = text_coords.Y + 55;
            text_coords.X = territory.System.Coordinates.X - (int)(output_str_length.X / 2);
            Color output_color = Color.White;

            if (territory.StarIndustry.OutboundPower == 0)
            {
                output_color = Color.Red;
            }

            assets.text.Draw(spriteBatch, text_coords, node_output);

            if (assets.text.IsHovering(text_coords, mouse_vec, node_output))
            {
                tooltipTrigger.Activate(StarAssets.Tooltip_status.Output, territory);
            }
        }

        private void DrawBottomNumber(SpriteBatch spriteBatch, SpriteFont font, Vector2 mouse_vec, Faction owner_fac, Territory territory)
        {
            Vector2 text_coords = territory.System.Coordinates;
            text_coords.Y = text_coords.Y - 35;

            if (owner_fac == Faction.player_fac)
            {
                DrawOutput(spriteBatch, font, mouse_vec, text_coords, territory);
            }
            else
            {
                double node_cost = territory.GetClaimCost(Faction.player_fac);
                node_cost = Math.Ceiling(node_cost);
                string node_cost_str = node_cost.ToString();
                Vector2 cost_str_length = font.MeasureString(node_cost_str);
                text_coords.Y = text_coords.Y + 55;
                text_coords.X = territory.System.Coordinates.X - (int)(cost_str_length.X / 2);

                spriteBatch.DrawString(
                    font,
                    node_cost_str,
                    text_coords,
                    Color.White);

                assets.text.Draw(spriteBatch, text_coords, node_cost_str);

                if (assets.text.IsHovering(text_coords, mouse_vec, node_cost_str))
                {
                    tooltipTrigger.Activate(StarAssets.Tooltip_status.Claim, territory);
                }
            }
        }

        private void DrawTopNumber(SpriteBatch spriteBatch, SpriteFont font, Vector2 mouse_vec, Territory star)
        {
            string node_prod = star.StarIndustry.Production.ToString();
            string investment_str = star.Investment.ToString();
            string top_number = node_prod + " | " + investment_str;
            Vector2 top_number_length = font.MeasureString(top_number);
            Vector2 text_coords = star.System.Coordinates;
            text_coords.Y = text_coords.Y - 35;
            text_coords.X = text_coords.X - (int)(top_number_length.X / 2);

            assets.text.Draw(spriteBatch, text_coords, top_number);

            if (assets.text.IsHovering(text_coords, mouse_vec, top_number))
            {
                tooltipTrigger.Activate(StarAssets.Tooltip_status.Top_number, star);
            }
        }

        private void DrawStarTextures(SpriteBatch spriteBatch, Vector2 mouse_vec, Territory selected_node, Vector2 star_pos, Faction owner_fac, Color color, Territory star)
        {
            spriteBatch.Draw(assets.star_texture, star_pos, color);

            //drawing capital star asset
            if (owner_fac != Faction.no_owner_fac)
            {
                if (owner_fac.Capital == star)
                {
                    Vector2 capital_pos = new Vector2((
                        star.System.Coordinates.X - 16),
                        (star.System.Coordinates.Y - 16));
                    spriteBatch.Draw(
                        assets.capital_star_texture,
                        capital_pos,
                        color);
                }
            }

            double dist = Vector2.Distance(star.System.Coordinates, mouse_vec);

            //draw hovering texture and set tooltip status
            if (dist < 10)
            {
                tooltipTrigger.Activate(StarAssets.Tooltip_status.Star, star);

                if (selected_node != star)
                {
                    star_pos.X = star.System.Coordinates.X - 8;
                    star_pos.Y = star.System.Coordinates.Y - 8;
                    spriteBatch.Draw(assets.hover_star_texture, star_pos, Color.White);
                }
            }
        }

        public void DrawStar(
            SpriteBatch spriteBatch,
            SpriteFont font,
            Vector2 mouse_vec,
            Territory selected_node,
            Territory territory)
        {
            Vector2 star_pos = Vector2.Add(territory.System.Coordinates, assets.star_texture_origin);
            Faction owner_fac = Faction.GetStarOwner(territory);

            Color color;

            color = owner_fac.Color;

            DrawStarTextures(spriteBatch, mouse_vec, selected_node, star_pos, owner_fac, color, territory);

            DrawTopNumber(spriteBatch, font, mouse_vec, territory);
            DrawBottomNumber(spriteBatch, font, mouse_vec, owner_fac, territory);
        }
    }

    internal class Cosmos
    {
        StarAssets assets;
        Star star;

        public Cosmos(StarAssets starAssets, Trigger tooltipTrigger)
        {
            assets = starAssets;
            star = new Star(assets, tooltipTrigger);
        }

        public void DrawLine(SpriteBatch spriteBatch, Vector2 start, Vector2 end, Color color)
        {
            Vector2 edge = end - start;

            float angle = (float)Math.Atan2(edge.Y, edge.X);

            spriteBatch.Draw(assets.pixel_texture,
                new Rectangle(
                    (int)start.X,
                    (int)start.Y,
                    (int)edge.Length(),
                    1),
                null,
                color,
                angle,
                new Vector2(0, 0),
                SpriteEffects.None,
                0);
        }

        private void DrawAsSelectedStar(SpriteBatch spriteBatch, Vector2 mouse_vec, Color lane_color, Vector2 coordinates)
        {
            Vector2 coords = coordinates;
            coords.X = coords.X - 12;
            coords.Y = coords.Y - 12;
            spriteBatch.Draw(assets.selected_star_texture, coords, Color.White);

            DrawLine(spriteBatch, coordinates, mouse_vec, lane_color);
        }

        private void DrawLane(SpriteBatch spriteBatch, Territory star)
        {
            if (star.StarIndustry.OutputTarget != star.StarIndustry)
            {
                Vector2 start = star.System.Coordinates;
                Vector2 end = star.StarIndustry.OutputTarget.Location.System.Coordinates;

                Color color;
                if (Faction.player_fac.owned_stars.Contains(star))
                {
                    color = Faction.player_fac.Lane_color;
                }
                else
                {
                    color = Faction.green_fac.Lane_color;
                }

                DrawLine(spriteBatch, star.System.Coordinates, end, color);
            }
        }

        public void DrawStars(SpriteBatch spriteBatch, Vector2 mouse_vec, Territory selected_star, SpriteFont small_font)
        {
            foreach (Territory node in Territory.territoryList)
            {
                DrawLane(spriteBatch, node);
            }

            if (selected_star != null)
            {
                DrawAsSelectedStar(spriteBatch, mouse_vec, Faction.player_fac.Lane_color, selected_star.System.Coordinates);
            }

            foreach (Territory node in Territory.territoryList)
            {
                star.DrawStar(spriteBatch, small_font, mouse_vec, selected_star, node);
            }
        }
    }
}