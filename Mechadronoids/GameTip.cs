﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Mechadronoids
{
    internal class TooltipStateMachine
    {
        public StarAssets.Tooltip_status tooltipStatus;
        StarTip starTip;

        public TooltipStateMachine()
        {
            tooltipStatus = StarAssets.Tooltip_status.Inactive;
            starTip = new StarTip();
        }

        private List<string> GetSelectedTooltipText(SpriteBatch spriteBatch, Vector2 mouse_vec, Territory selectedTerritory)
        {
            List<string> text_list = new List<string>();

            double dist = selectedTerritory.System.DistanceFrom(mouse_vec);
            double cost = selectedTerritory.System.GetDistCost(dist);
            text_list.Add("Distance cost: " + cost.ToString("F"));

            return text_list;
        }

        private List<string> GetFactionTooltipText(Faction faction)
        {
            List<string> text_list = new List<string>();
            text_list.Add("Money: " + faction.Money.ToString("F") + " +" + faction.GetIncome().ToString("F"));
            text_list.Add("----------");
            text_list.Add("Claim more stars and");
            text_list.Add("connect them to your");
            text_list.Add("capital to get more");
            text_list.Add("money per turn");
            return text_list;
        }

        public void UpdateTooltipStatus(SpriteBatch spriteBatch, Vector2 mouse_vec, Tooltip tooltip, Territory selectedTerritory, Trigger tooltipTrigger)
        {
            tooltipStatus = tooltipTrigger.tooltipType;
            Territory territory = tooltipTrigger.territory;

            switch (tooltipStatus)
            {
                case StarAssets.Tooltip_status.Top_number:
                    tooltip.SetActive(spriteBatch, starTip.GetTopTooltipText(territory), mouse_vec);
                    break;
                case StarAssets.Tooltip_status.Star:
                    tooltip.SetActive(spriteBatch, starTip.GetStarTooltipText(territory), mouse_vec);
                    break;
                case StarAssets.Tooltip_status.Output:
                    tooltip.SetActive(spriteBatch, starTip.GetOutputTooltipText(territory), mouse_vec);
                    break;
                case StarAssets.Tooltip_status.Claim:
                    tooltip.SetActive(spriteBatch, starTip.GetClaimTooltipText(territory), mouse_vec);
                    break;
                case StarAssets.Tooltip_status.Selected:
                    tooltip.SetActive(spriteBatch, GetSelectedTooltipText(spriteBatch, mouse_vec, selectedTerritory), mouse_vec);
                    break;
                case StarAssets.Tooltip_status.Money:
                    tooltip.SetActive(spriteBatch, GetFactionTooltipText(Faction.player_fac), mouse_vec);
                    break;
                default:
                    break;
            }

            tooltipStatus = StarAssets.Tooltip_status.Inactive;
            tooltipTrigger.Reset();
        }
    }

    internal class Trigger
    {
        public StarAssets.Tooltip_status tooltipType;
        public Territory territory;

        public Trigger()
        {
            tooltipType = StarAssets.Tooltip_status.Inactive;
            territory = null;
        }

        public void Activate(StarAssets.Tooltip_status tooltip_status, Territory targetTerritory)
        {
            tooltipType = tooltip_status;
            territory = targetTerritory;
        }

        public void Reset()
        {
            tooltipType = StarAssets.Tooltip_status.Inactive;
            territory = null;
        }
    }

    internal class StarTip
    {
        public StarTip()
        {

        }

        private void GetOwnerStarTooltipText(List<string> result, Territory star)
        {
            if (Faction.player_fac.Capital != star)
            {
                result.Add("Owned star.");
                result.Add("-------");
                result.Add("[Left-Click] - selects star");
                result.Add("[Left-Click] + [star selected] - this star becomes output target");
                result.Add("[Right-Click] - increases investment by 10, costs 10");
                result.Add("");
                result.Add("Be sure to connect it");
                result.Add("to the capital. Either");
                result.Add("directly or by using");
                result.Add("other owned stars as");
                result.Add("mid-way stations.");
            }
            else
            {
                result.Add("Your capital");
                result.Add("-------");
                result.Add("[Right-Click] - increases investment by 10, costs 10");
                result.Add("");
                result.Add("Send as much power into");
                result.Add("this star as possible.");
            }
        }

        public List<string> GetStarTooltipText(Territory star)
        {
            List<string> result = new List<string>();

            if (Faction.player_fac.owned_stars.Contains(star))
            {
                GetOwnerStarTooltipText(result, star);
            }
            else if (Faction.no_owner_fac.owned_stars.Contains(star))
            {
                result.Add("Unowned star.");
                result.Add("-------");
                result.Add("[Left-Click] - claims this star");
                result.Add("");
                result.Add("Claim this star to get");
                result.Add("this star's production.");
            }
            else
            {
                result.Add("Enemy star");
                result.Add("-------");
                result.Add("[Left-Click] - claims this star");
                result.Add("");
                result.Add("Claim their stars before");
                result.Add("they claim your stars.");
            }

            return result;
        }

        public List<string> GetTopTooltipText(Territory star)
        {
            List<string> result = new List<string>();

            string line = "Production: +" + ((int)star.StarIndustry.Production).ToString();
            result.Add(line);

            result.Add("");
            result.Add("This is the money produced");
            result.Add("on this star.");

            result.Add("-------");

            line = "Investment: " + ((int)star.Investment).ToString();
            result.Add(line);
            result.Add("");
            result.Add("[Right-Click] - increases investment by 10, costs 10");
            result.Add("");
            result.Add("Amount of defences that have");
            result.Add("been invested in. Investments");
            result.Add("make it more expensive to claim.");

            return result;
        }

        public List<string> GetOutputTooltipText(Territory star)
        {
            List<string> result = new List<string>();

            string line = "Output: +" + star.StarIndustry.OutboundPower.ToString("F");
            result.Add(line);

            result.Add("+ " + star.StarIndustry.Production.ToString("F") + " produced");
            result.Add("+ " + star.StarIndustry.LastInbound.ToString("F") + " incoming");
            result.Add("- " + star.StarIndustry.Port.GetOutputDistCost(star, star.StarIndustry.OutputTarget).ToString("F") + " distance cost");

            result.Add("");
            result.Add("[Left-Click] - selects star");
            result.Add("[Left-Click] + [star selected] - this star becomes output target");

            return result;
        }

        public List<string> GetClaimTooltipText(Territory star)
        {
            List<string> result = new List<string>();

            string line = "Claim cost: " + star.GetClaimCost(Faction.player_fac).ToString("F");
            result.Add(line);

            result.Add("+ " + star.Base_claim_cost.ToString("F") + " base claim cost");
            result.Add("+ " + star.System.GetClaimCostNearest(Faction.player_fac).ToString("F") + " distance from nearest star");
            result.Add("+ " + star.GetClaimCostCapital(Faction.player_fac).ToString("F") + " distance from capital");
            result.Add("+ " + star.Investment.ToString("F") + " investment");

            return result;
        }
    }
}