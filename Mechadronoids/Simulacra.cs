﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Mechadronoids
{
    public class Territory
    {
        public static List<Territory> territoryList;

        private double investment;
        private Industry industry;
        private StellarSystem system;
        private double base_claim_cost;

        public double Investment { get => investment; set => investment = value; }
        public double Investment1 { get => investment; set => investment = value; }
        internal Industry StarIndustry { get => industry; set => industry = value; }
        internal StellarSystem System { get => system; set => system = value; }
        public double Base_claim_cost { get => base_claim_cost; set => base_claim_cost = value; }

        public Territory(double power_gen, Vector2 coords)
        {
            investment = 0;
            industry = new Industry(this, power_gen);
            system = new StellarSystem(coords);
        }

        public double GetClaimCost(Faction fac)
        {
            double distance_cost = system.GetClaimCostNearest(fac);
            double capital_dist_cost = GetClaimCostCapital(fac);

            double cost = base_claim_cost + distance_cost + investment + capital_dist_cost;

            return cost;
        }

        public void SetOwner()
        {
            industry.ResetInputOutput();
            investment = 0;
        }

        public void Invest(Faction fac)
        {
            double cost = GetInvestCost(fac);
            if (fac.owned_stars.Contains(this))
            {
                if (fac.Money >= cost)
                {
                    investment = investment + cost;
                    fac.Money = fac.Money - cost;
                }
            }
        }

        public double GetInvestCost(Faction fac)
        {
            return 10;
        }

        public double GetClaimCostCapital(Faction fac)
        {
            double distance_from_capital = Vector2.Distance(fac.Capital.system.Coordinates, system.Coordinates);
            double capital_dist_cost = distance_from_capital * 0.2;

            return capital_dist_cost;
        }
    }

    public class Faction
    {
        public static Faction player_fac;
        public static Faction green_fac;
        public static Faction no_owner_fac;

        private readonly Color color;
        private readonly Color lane_color;
        private Territory capital;
        private double money;
        private bool active;
        private Text text_obj;
        internal List<Territory> owned_stars;

        internal Faction(Color color_inp, Color lane_color_inp, SpriteFont font)
        {
            color = color_inp;
            lane_color = lane_color_inp;
            capital = null;
            money = 50;
            active = true;
            text_obj = new Text(font);
            owned_stars = new List<Territory>();
        }

        internal Color Color => color;
        internal Territory Capital { get => capital; set => capital = value; }
        internal double Money { get => money; set => money = value; }
        internal bool Active { get => active; set => active = value; }
        internal Color Lane_color => lane_color;
        public Text Text_obj { get => text_obj; set => text_obj = value; }

        public void AddMoney(double added_money)
        {
            money = money + added_money;
        }

        public void Update()
        {
            if (active == true)
            {
                if (owned_stars.Contains(capital) == false)
                {
                    active = false;
                    return;
                }

                double collected_money = capital.StarIndustry.OutboundPower;
                AddMoney(collected_money);
            }
            else
            {
                money = 0;
            }
        }

        public double GetIncome()
        {
            double income = Capital.StarIndustry.OutboundPower;
            return income;
        }

        public void ClaimStar(Territory star)
        {
            double claim_cost = star.GetClaimCost(this);

            if (claim_cost < money)
            {
                money = money - claim_cost;
                star.SetOwner();

                if (player_fac.owned_stars.Contains(star))
                {
                    player_fac.UnClaimStar(star);
                }
                else if (green_fac.owned_stars.Contains(star))
                {
                    green_fac.UnClaimStar(star);
                }
                else
                {
                    no_owner_fac.UnClaimStar(star);
                }

                owned_stars.Add(star);
            }
        }

        public void UnClaimStar(Territory star)
        {
            owned_stars.Remove(star);
        }

        public static Faction GetStarOwner(Territory star)
        {
            Faction owner_fac;

            if (Faction.player_fac.owned_stars.Contains(star))
            {
                owner_fac = Faction.player_fac;
            }
            else if (Faction.green_fac.owned_stars.Contains(star))
            {
                owner_fac = Faction.green_fac;
            }
            else
            {
                owner_fac = Faction.no_owner_fac;
            }

            return owner_fac;
        }
    }

    internal class Simulacra
    {
        public Simulacra(Assets assets)
        {
            Faction.player_fac = new Faction(Color.CornflowerBlue, Color.SandyBrown, assets.font_large);
            Faction.green_fac = new Faction(Color.Green, Color.DarkRed, assets.font_large);

            InitStars(assets);
        }

        public void Update()
        {
            UpdateStars();
            Faction.player_fac.Update();
            Faction.green_fac.Update();
        }

        private void UpdateStars()
        {
            foreach (Territory node in Territory.territoryList)
            {
                node.StarIndustry.Flip();
            }

            foreach (Territory node in Territory.territoryList)
            {
                if (Faction.no_owner_fac.owned_stars.Contains(node) == false)
                {
                    node.StarIndustry.Update();
                }
            }
        }

        private void SetStartingStar(Faction faction, Territory star)
        {
            faction.Capital = star;
            Faction.no_owner_fac.owned_stars.Remove(star);
            star.SetOwner();
            faction.owned_stars.Add(star);
            star.StarIndustry.Production = 10;
        }

        private void GenerateStars()
        {
            Random rnd = new Random();

            for (int i = 0; i < 100; i++)
            {
                int rank = i + 1;

                double power_gen = rnd.Next(1, 10);

                bool valid_point = false;
                Vector2 coords = new Vector2(0, 0);
                while (valid_point == false)
                {
                    valid_point = true;
                    int X = rnd.Next(50, 1850);
                    int Y = rnd.Next(50, 950);
                    coords = new Vector2(X, Y);


                    foreach (Territory item in Territory.territoryList)
                    {
                        double dist = item.System.DistanceFrom(coords);

                        if (dist < 70)
                        {
                            valid_point = false;
                            break;
                        }
                    }
                }

                Territory node = new Territory(power_gen, coords);
                Territory.territoryList.Add(node);
            }
        }

        private Territory FindStartingStar(Vector2 ideal_coords)
        {
            Territory most_suitable_star = null;
            double distance = 999999;
            foreach (Territory star in Territory.territoryList)
            {
                if (Faction.no_owner_fac.owned_stars.Contains(star))
                {
                    if (most_suitable_star == null)
                    {
                        most_suitable_star = star;
                        distance = most_suitable_star.System.DistanceFrom(ideal_coords);
                    }
                    else if (star.System.DistanceFrom(ideal_coords) < distance)
                    {
                        most_suitable_star = star;
                        distance = star.System.DistanceFrom(ideal_coords);
                    }
                }
            }

            return most_suitable_star;
        }

        private void InitStars(Assets assets)
        {
            Territory.territoryList = new List<Territory>();

            Faction.no_owner_fac = new Faction(Color.White, Color.White, assets.font_large);

            GenerateStars();

            foreach (Territory star in Territory.territoryList)
            {
                Faction.no_owner_fac.owned_stars.Add(star);
            }

            Vector2 ideal_coords = new Vector2(200, 500);
            Territory starting_star = FindStartingStar(ideal_coords);
            SetStartingStar(Faction.player_fac, starting_star);

            ideal_coords = new Vector2(1680, 500);
            starting_star = FindStartingStar(ideal_coords);
            SetStartingStar(Faction.green_fac, starting_star);
        }
    }

    internal class Industry
    {
        double production;
        Industry outputTarget;
        Territory location;
        double lastInbound;
        Port port;

        public Industry(Territory star, double prod)
        {
            outputTarget = this;
            location = star;
            production = prod;
            lastInbound = 0;
            port = new Port();
        }

        public double InboundPower { get => port.inboundPower; }
        public double OutboundPower { get => port.outboundPower; set => port.outboundPower = value; }
        internal Industry OutputTarget { get => outputTarget; set => outputTarget = value; }
        public Territory Location { get => location; set => location = value; }
        public double Production { get => production; set => production = value; }
        public double LastInbound { get => lastInbound; set => lastInbound = value; }
        internal Port Port { get => port; }

        public void Update()
        {
            double imports = port.TakeInbound();
            lastInbound = imports;

            double total = production + imports;

            port.NodeOutput(total, port.GetOutputDistCost(location, outputTarget));
        }

        public void NodeInput(double power)
        {
            port.inboundPower = port.inboundPower + power;
        }

        public void ResetInputOutput()
        {
            outputTarget = this;

            foreach (Territory node in Territory.territoryList)
            {
                if (node.StarIndustry.OutputTarget == this)
                {
                    node.StarIndustry.OutputTarget = node.StarIndustry;
                }
            }
        }

        public void Flip()
        {
            if (outputTarget != this)
            {
                outputTarget.NodeInput(port.TakeOutbound());
            }
        }
    }

    internal class StellarSystem
    {
        Vector2 coordinates;

        public StellarSystem(Vector2 coords)
        {
            coordinates = coords;
        }

        public Vector2 Coordinates { get => coordinates; set => coordinates = value; }

        public double DistanceFrom(StellarSystem system)
        {
            double dist = Vector2.Distance(coordinates, system.coordinates);
            return dist;
        }

        public double DistanceFrom(Vector2 star_vec)
        {
            double dist = Vector2.Distance(coordinates, star_vec);
            return dist;
        }

        public double GetDistCost(double dist)
        {
            double cost = dist * 0.01;
            return cost;
        }

        public double GetClaimCostNearest(Faction fac)
        {
            Vector2 fac_cap_coords = fac.Capital.System.Coordinates;
            double distance = Vector2.Distance(Coordinates, fac_cap_coords);
            double shortest_distance = distance;

            Vector2 node_cor;
            foreach (Territory node in Territory.territoryList)
            {
                if (fac.owned_stars.Contains(node))
                {
                    node_cor = node.System.Coordinates;
                    distance = Vector2.Distance(node_cor, Coordinates);

                    if (distance < shortest_distance)
                    {
                        shortest_distance = distance;
                    }
                }
            }

            double distance_cost = Math.Pow(2, (shortest_distance * 0.015));

            return distance_cost;
        }
    }

    internal class Port
    {
        internal double inboundPower;
        internal double outboundPower;

        public Port()
        {
            inboundPower = 0;
        }

        public double TakeInbound()
        {
            double power = inboundPower;
            inboundPower = 0;
            return power;
        }

        public double TakeOutbound()
        {
            double power = outboundPower;
            outboundPower = 0;
            return power;
        }

        public void NodeOutput(double power, double dist_cost)
        {
            double output_content = power;

            double cost = dist_cost;

            output_content = output_content - cost;

            if (output_content < 0)
            {
                output_content = 0;
            }

            outboundPower = output_content;
        }

        public double GetOutputDistCost(Territory location, Industry outputTarget)
        {
            double dist = location.System.DistanceFrom(outputTarget.Location.System);
            double cost = location.System.GetDistCost(dist);
            return cost;
        }
    }
}