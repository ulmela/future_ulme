﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Mechadronoids
{
    internal class UserInterface
    {
        Assets assets;
        Matrix res_scale;
        bool play_music;
        Song next_song;
        GraphicsDeviceManager graphics;
        GameScreen gameScreen;

        public UserInterface(Command com_nextTurn, Command com_exit, Assets UI_assets, Tooltip UI_tooltip, bool play_UI_music, GraphicsDeviceManager UI_graphics, StarAssets star_assets)
        {
            assets = UI_assets;
            play_music = play_UI_music;
            next_song = assets.song_1;
            graphics = UI_graphics;

            Resolution res_virt = new Resolution(1920, 1080);

            int actual_width = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            int actual_height = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;

            Resolution res_actual = new Resolution(actual_width, actual_height);

            float scaleX = (float)res_actual.width / (float)res_virt.width;
            float scaleY = (float)res_actual.height / (float)res_virt.height;
            Vector3 scale = new Vector3(scaleX, scaleY, 1.0f);
            res_scale = Matrix.CreateScale(scale);

            graphics.ToggleFullScreen();
            graphics.PreferredBackBufferHeight = res_virt.height;
            graphics.PreferredBackBufferWidth = res_virt.width;
            graphics.ApplyChanges();

            Command play_click = new Command(PlayClick);
            gameScreen = new GameScreen(assets.game_map_texture, UI_tooltip, star_assets, com_nextTurn, com_exit, play_click);
        }

        public void Update()
        {
            UpdateMusic();
        }

        private void PlayClick()
        {
            assets.click_sfx.Play(0.02f, 0.0f, 0.0f);
        }

        public void DrawUI(SpriteBatch spriteBatch, ScreenType screen)
        {
            graphics.GraphicsDevice.Clear(Color.Black);

            Point mouse_point = Mouse.GetState().Position;
            Vector2 mouse_vec_raw = new Vector2(mouse_point.X, mouse_point.Y);
            Vector2 mouse_vec = Vector2.Transform(mouse_vec_raw, Matrix.Invert(res_scale));

            spriteBatch.Begin(transformMatrix: res_scale);

            if (screen == ScreenType.Game)
            {
                gameScreen.Draw(spriteBatch, mouse_vec, assets.font_8, res_scale);
            }

            else if (screen == ScreenType.Title)
            {
                DrawTitleScreen(spriteBatch);
            }

            else if (screen == ScreenType.Intro)
            {
                DrawIntroScreen(spriteBatch);
            }

            else if (screen == ScreenType.End)
            {
                DrawEndScreen(spriteBatch);
            }

            spriteBatch.End();
        }

        private void DrawTitleScreen(SpriteBatch spriteBatch)
        {
            Vector2 game_map_pos = new Vector2(0, 0);
            spriteBatch.Draw(assets.title_texture,
                game_map_pos,
                Color.White);
        }

        private void DrawIntroScreen(SpriteBatch spriteBatch)
        {
            Vector2 game_map_pos = new Vector2(0, 0);
            spriteBatch.Draw(assets.intro_texture,
                game_map_pos,
                Color.White);
        }

        private void DrawEndScreen(SpriteBatch spriteBatch)
        {
            graphics.GraphicsDevice.Clear(Color.Black);

            string text = "The game has ended";
            Vector2 pos = new Vector2(850, 500);
            spriteBatch.DrawString(assets.font_large, text, pos, Color.White);

            if (Faction.green_fac.Active == false)
            {
                text = "You have WON!";
            }
            else
            {
                text = "You have been defeated!";
            }
            pos = pos + new Vector2(0, 20);
            spriteBatch.DrawString(assets.font_large, text, pos, Color.White);

            text = "Press [Esc] to exit";
            pos = pos + new Vector2(0, 40);
            spriteBatch.DrawString(assets.font_large, text, pos, Color.White);
        }

        public void UpdateMusic()
        {
            if (play_music == true)
            {
                if (MediaPlayer.State == MediaState.Stopped)
                {
                    if (next_song == assets.song_1)
                    {
                        MediaPlayer.Play(assets.song_1);
                        next_song = assets.song_2;
                    }
                    else if (next_song == assets.song_2)
                    {
                        MediaPlayer.Play(assets.song_2);
                        next_song = assets.song_1;
                    }
                }
            }
        }
    }
    
    internal enum ScreenType
    {
        Title,
        Intro,
        Game,
        End
    }

    internal class Assets
    {
        public Texture2D game_map_texture;
        public Texture2D title_texture;
        public Texture2D intro_texture;
        public Song song_1;
        public Song song_2;
        public SoundEffect click_sfx;
        public SpriteFont font_8;
        public SpriteFont font_large;

        public Assets(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            game_map_texture = Content.Load<Texture2D>("galaxy");
            title_texture = Content.Load<Texture2D>("ulme_title");
            intro_texture = Content.Load<Texture2D>("ulme_intro");
            song_1 = Content.Load<Song>("Vector_Horizon");
            song_2 = Content.Load<Song>("Circuitry_Solitude");
            click_sfx = Content.Load<SoundEffect>("click");
            font_8 = Content.Load<SpriteFont>("Info");
            font_large = Content.Load<SpriteFont>("LargeFont");
        }
    }
    
    struct Resolution
    {
        public int width;
        public int height;

        public Resolution(int w, int h)
        {
            width = w;
            height = h;
        }
    }
}