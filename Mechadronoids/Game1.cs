﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Mechadronoids
{
    public class Game1 : Game
    {
        bool dev_mode;
        int turn_count;
        ScreenType screen;
        Assets assets;
        StarAssets starAssets;
        UserInterface UI;
        Simulacra sim;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        AI green_AI;
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Draw(GameTime gameTime)
        {
            UI.DrawUI(spriteBatch, screen);

            base.Draw(gameTime);
        }
        
        protected override void Update(GameTime gameTime)
        {
            UI.Update();

            base.Update(gameTime);
        }

        protected override void Initialize()
        {
            base.Initialize();

            dev_mode = true;
            bool play_music;

            if (dev_mode == true)
            {
                play_music = false;
                screen = ScreenType.Game;
            }
            else
            {
                play_music = true;
                screen = ScreenType.Title;
            }

            Tooltip tooltip = new Tooltip(assets.font_8);
            sim = new Simulacra(assets);
            
            green_AI = new AI(Faction.green_fac);

            Command command_next_turn = new Command(NextTurn);
            Command command_exit = new Command(Exit);

            UI = new UserInterface(command_next_turn, command_exit, assets, tooltip, play_music, graphics, starAssets);
            IsMouseVisible = true;

            sim.Update();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Texture2D pixel = new Texture2D(GraphicsDevice, 1, 1);
            pixel.SetData(new[] { Color.White });
            Tooltip.background_tex = pixel;

            assets = new Assets(Content);

            starAssets = new StarAssets(
                Content,
                GraphicsDevice,
                new Tooltip(assets.font_8),
                new Text(assets.font_8));

            StarAssets.starAssets = starAssets;
        }

        private void NextTurn()
        {
            if (screen == ScreenType.Game)
            {
                sim.Update();

                if ((Faction.player_fac.Active == false) || (Faction.green_fac.Active == false))
                {
                    screen = ScreenType.End;
                    return;
                }
                else
                {
                    green_AI.Update();
                    turn_count = turn_count + 1;
                }
            }
            else if (screen == ScreenType.Title)
            {
                screen = ScreenType.Intro;
            }
            else if (screen == ScreenType.Intro)
            {
                screen = ScreenType.Game;
            }
        }
    }

    public class AI
    {
        private Faction faction;
        private bool can_spend_money;
        private double invest_score;
        private double invest_score_base_growth;
        private double claim_score;
        private double claim_score_base_growth;
        Expander obtainer;

        public Faction Faction { get => faction; set => faction = value; }
        public bool Can_spend_money { get => can_spend_money; set => can_spend_money = value; }

        public AI(Faction fac)
        {
            faction = fac;
            can_spend_money = true;
            invest_score = 0;
            invest_score_base_growth = 10;
            claim_score = 1;
            claim_score_base_growth = 10;
            obtainer = new Expander(this);
        }

        public void Update()
        {
            if ((faction.Active == true) && (Faction.player_fac.Active == true))
            {
                can_spend_money = true;

                while ((can_spend_money == true) && (Faction.player_fac.owned_stars.Count > 0))
                {
                    UpdateActionScores();

                    if (claim_score > invest_score)
                    {
                        obtainer.ClaimStar();
                        if (can_spend_money == true)
                        {
                            claim_score = 0;
                        }
                    }
                    else
                    {
                        Invest();
                        if (can_spend_money == true)
                        {
                            invest_score = 0;
                        }
                    }
                }

                ArrangePowerTransfer();
            }
        }

        private void UpdateActionScores()
        {
            double total_stars = Territory.territoryList.Count;
            double unowned_stars = 0;

            foreach (Territory star in Territory.territoryList)
            {
                if (Faction.no_owner_fac.owned_stars.Contains(star))
                {
                    unowned_stars = unowned_stars + 1;
                }
            }

            double unowned_to_total_ratio = unowned_stars / total_stars;
            double claim_modifier = unowned_to_total_ratio * 10;
            claim_score = claim_score + (claim_score_base_growth * claim_modifier + 1);

            Territory closest_enemy_to_capital = FindNearestStar(Faction.player_fac.owned_stars, faction.Capital);
            double closest_dist = closest_enemy_to_capital.System.DistanceFrom(faction.Capital.System);
            double invest_modifier = 30 / (closest_dist / 100);
            invest_score = invest_score + (invest_score_base_growth * invest_modifier);
        }

        private void ArrangePowerTransfer()
        {
            List<Territory> unconnected_star_list = new List<Territory>();
            List<Territory> connected_star_list = new List<Territory>();
            connected_star_list.Add(faction.Capital);

            foreach (Territory node in Territory.territoryList)
            {
                if (faction.owned_stars.Contains(node))
                {
                    if (faction.Capital != node)
                    {
                        unconnected_star_list.Add(node);
                    }
                }
            }

            while (unconnected_star_list.Count > 0)
            {
                Territory nearest_out_star, nearest_in_star;
                FindPotentialConnection(unconnected_star_list, connected_star_list, out nearest_out_star, out nearest_in_star);

                if (nearest_out_star.StarIndustry.OutputTarget != nearest_in_star.StarIndustry)
                {
                    nearest_out_star.StarIndustry.OutputTarget = nearest_in_star.StarIndustry;
                }

                unconnected_star_list.Remove(nearest_out_star);
                connected_star_list.Add(nearest_out_star);
            }
        }

        private void FindPotentialConnection(List<Territory> unconnected_node_list, List<Territory> connected_node_list, out Territory nearest_out_star, out Territory nearest_in_star)
        {
            nearest_out_star = null;
            nearest_in_star = null;
            double distance = 999999999;

            foreach (Territory node in unconnected_node_list)
            {
                if (nearest_out_star == null)
                {
                    nearest_out_star = node;
                    nearest_in_star = FindNearestStar(connected_node_list, node);
                    distance = nearest_out_star.System.DistanceFrom(nearest_in_star.System);
                }
                else
                {
                    Territory new_nearest_in_star = FindNearestStar(connected_node_list, node);

                    double new_distance = node.System.DistanceFrom(new_nearest_in_star.System);

                    if (distance > new_distance)
                    {
                        nearest_out_star = node;
                        nearest_in_star = new_nearest_in_star;
                        distance = new_distance;
                    }
                }
            }
        }

        private Territory FindNearestStar(List<Territory> star_list, Territory focus_star)
        {
            Territory nearest_star = null;

            foreach (Territory star in star_list)
            {
                if (nearest_star == null)
                {
                    nearest_star = star;
                }
                else
                {
                    double distance_to_current = focus_star.System.DistanceFrom(nearest_star.System);
                    double distance_to_new = focus_star.System.DistanceFrom(star.System);

                    if (distance_to_current > distance_to_new)
                    {
                        nearest_star = star;
                    }
                }
            }

            return nearest_star;
        }

        private void Invest()
        {
            if (faction.Money < 10)
            {
                can_spend_money = false;
                return;
            }

            Territory best_investment = FindBestInvestment();

            best_investment.Invest(faction);
        }

        private Territory FindBestInvestment()
        {
            Territory best_investment = null;
            double best_score = 9999;

            foreach (Territory star in Territory.territoryList)
            {
                if (faction.owned_stars.Contains(star))
                {
                    double production = star.StarIndustry.Production;
                    double investment = star.Investment;

                    if (investment == 0)
                    {
                        investment = 1;
                    }

                    double invest_prod_ratio = investment / production;

                    if (faction.Capital == star)
                    {
                        invest_prod_ratio = invest_prod_ratio / 32;
                    }

                    if (best_investment == null)
                    {
                        best_investment = star;
                        best_score = invest_prod_ratio;
                    }
                    else if (best_score > invest_prod_ratio)
                    {
                        best_investment = star;
                        best_score = invest_prod_ratio;
                    }
                }
            }

            return best_investment;
        }
    }

    internal class Expander
    {
        AI parent;
        Territory desiredTerritory;
        bool collecting;
        int maxSavingDuration;
        int savingDuration;
        Prospector prospector;

        public Expander(AI parent_of)
        {
            parent = parent_of;
            desiredTerritory = null;
            collecting = false;
            maxSavingDuration = 3;
            savingDuration = maxSavingDuration;
            prospector = new Prospector(parent.Faction);
        }

        public Territory DesiredTerritory { get => desiredTerritory; set => desiredTerritory = value; }

        public void ClaimStar()
        {
            if (collecting == false)
            {
                desiredTerritory = prospector.FindStar();

                if (DesiredTerritory == null)
                {
                    parent.Can_spend_money = false;
                    return;
                }
            }

            if (parent.Faction.Money > DesiredTerritory.GetClaimCost(parent.Faction))
            {
                BuyStar(DesiredTerritory);
                collecting = false;
                DesiredTerritory = null;
                parent.Can_spend_money = true;
            }
            else
            {
                collecting = true;
                parent.Can_spend_money = false;
                savingDuration = savingDuration - 1;
                if (savingDuration < 1)
                {
                    savingDuration = maxSavingDuration;
                    collecting = false;
                    DesiredTerritory = null;
                }
            }
        }

        private void BuyStar(Territory star)
        {
            parent.Faction.ClaimStar(star);
        }
    }

    internal class Prospector
    {
        Faction faction;

        public Prospector(Faction fac)
        {
            faction = fac;
        }

        private Territory FindCheapestStar(List<Territory> star_list)
        {
            Territory smallest_price_star = null;

            foreach (Territory star in star_list)
            {

                if (smallest_price_star == null)
                {
                    smallest_price_star = star;
                }
                else
                {
                    if (star.GetClaimCost(faction) < smallest_price_star.GetClaimCost(faction))
                    {
                        smallest_price_star = star;
                    }
                }

            }

            return smallest_price_star;
        }

        private Territory FindClosestFar(List<Territory> best_stars)
        {
            Territory chosen_star = null;
            double chosen_dist = 99999999;

            foreach (Territory star in best_stars)
            {
                double dist = faction.Capital.System.DistanceFrom(star.System);

                if (chosen_star == null)
                {
                    chosen_star = star;
                    chosen_dist = dist;
                }
                else if (chosen_dist > dist)
                {
                    chosen_star = star;
                    chosen_dist = dist;
                }
            }

            return chosen_star;
        }

        private Territory FindTheBestStar(List<Territory> best_stars)
        {
            int enemy_count = 0;
            int self_count = 0;
            foreach (Territory star in Territory.territoryList)
            {
                if (Faction.no_owner_fac.owned_stars.Contains(star) == false)
                {
                    if (faction.owned_stars.Contains(star))
                    {
                        self_count = self_count + 1;
                    }
                    else
                    {
                        enemy_count = enemy_count + 1;
                    }
                }
            }

            Territory best_star;

            if ((enemy_count * 0.7) >= self_count)
            {
                best_star = FindCheapestStar(best_stars);
            }
            else
            {
                best_star = FindClosestFar(best_stars);
            }

            return best_star;
        }

        private List<Territory> FindAffordableStars()
        {
            List<Territory> star_list = new List<Territory>()
;
            double star_cost;
            double budget = faction.Money + (faction.GetIncome() * 3);

            foreach (Territory star in Territory.territoryList)
            {
                if (faction.owned_stars.Contains(star) == false)
                {
                    star_cost = star.GetClaimCost(faction);

                    if (star_cost < budget)
                    {
                        star_list.Add(star);
                    }
                }
            }

            return star_list;
        }

        private List<Territory> FindPotentialBestStars(List<Territory> star_list)
        {
            List<Territory> best_star_list = new List<Territory>();

            Territory best_star = null;

            foreach (Territory star in star_list)
            {
                if (best_star == null)
                {
                    best_star = star;
                }
                else if (best_star.StarIndustry.Production < star.StarIndustry.Production)
                {
                    best_star = star;
                }
            }

            foreach (Territory star in star_list)
            {
                if (star.StarIndustry.Production == best_star.StarIndustry.Production)
                {
                    best_star_list.Add(star);
                }
            }

            return best_star_list;
        }

        public Territory FindStar()
        {
            Territory desiredTerritory = null;

            List<Territory> affordable_stars;
            affordable_stars = FindAffordableStars();

            List<Territory> best_stars = new List<Territory>();
            if (affordable_stars.Count > 0)
            {
                best_stars = FindPotentialBestStars(affordable_stars);
            }
            else
            {
                desiredTerritory = null;
                return desiredTerritory;
            }

            if (best_stars.Count == 1)
            {
                desiredTerritory = best_stars[0];

            }
            else if (best_stars.Count > 1)
            {
                desiredTerritory = FindTheBestStar(best_stars);
            }

            return desiredTerritory;
        }
    }
}
