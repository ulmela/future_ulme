﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Mechadronoids
{
    public class Tooltip
    {
        public static Texture2D background_tex;

        List<string> text;
        Vector2 pos;
        public bool active;
        SpriteBatch spriteBatch;
        SpriteFont font;
        int line_step;

        public Tooltip(SpriteFont f)
        {
            text = new List<string>();
            pos = new Vector2(0, 0);
            active = false;
            line_step = 10;
            font = f;
        }

        public void Draw()
        {
            pos.X = pos.X + 15;
            Vector2 text_size = GetTextSize() + new Vector2(10, 10);
            Vector2 rect_pos = pos + new Vector2(-5, -5);
            Rectangle rect = new Rectangle(rect_pos.ToPoint(), text_size.ToPoint());
            spriteBatch.Draw(background_tex, rect, Color.Brown);

            foreach (string line in text)
            {
                spriteBatch.DrawString(font, line, pos, Color.White);
                pos.Y = pos.Y + line_step;
            }

            active = false;
        }

        public void SetActive(SpriteBatch sb, List<string> txt, Vector2 p)
        {
            spriteBatch = sb;
            text = txt;
            pos = p;
            active = true;
        }

        private Vector2 GetTextSize()
        {
            Vector2 final_size;
            float widest = 0;
            float tallest = 0;
            Vector2 size;

            foreach (string line in text)
            {
                size = font.MeasureString(line);

                if (size.X > widest)
                {
                    widest = size.X;
                }

                tallest = tallest + line_step;
            }

            final_size = new Vector2(widest, tallest);

            return final_size;
        }
    }

    public class Text
    {
        SpriteFont font;

        public Text(SpriteFont f)
        {
            font = f;
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 pos, string text)
        {
            spriteBatch.DrawString(font, text, pos, Color.White);
        }

        public bool IsHovering(Vector2 self_pos, Vector2 mouse_pos, string text)
        {
            bool result = false;

            Vector2 text_size = font.MeasureString(text);
            float left_X = self_pos.X;
            float right_X = left_X + text_size.X;
            float top_Y = self_pos.Y;
            float bot_Y = top_Y + text_size.Y;

            if ((mouse_pos.X > left_X) &&
                (mouse_pos.X < right_X) &&
                (mouse_pos.Y > top_Y) &&
                (mouse_pos.Y < bot_Y))
            {
                result = true;
            }

            return result;
        }
    }

    public class Command
    {
        private Action command_func;

        public Command(Action func)
        {
            command_func = func;
        }

        public void Execute()
        {
            command_func();
        }
    }

    public class TargetedCommand
    {
        private Action<Territory> command_func;

        public TargetedCommand(Action<Territory> func)
        {
            command_func = func;
        }

        public void Execute(Territory target)
        {
            command_func(target);
        }
    }
}